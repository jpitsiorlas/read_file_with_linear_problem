file_name = input('Enter the name of the file: ')

# reading the file
try:
    with open(file_name,'r', encoding='utf-8') as f:
        char_list = []
        line_counter = []
        for line in f:
            for ch in line:
                if ch == ' ' or ch == '\t':
                    continue
                else:
                    char_list.append(ch)

except EOFError:
    print('Error with the file')
    exit(1)

# finding if the word end exists in the problem

if char_list[-1] == 'D' or char_list[-1] == 'd' and char_list[-2] == 'N' or char_list[-2] =='n'\
and char_list[-3] == 'e' or char_list[-3] == 'E':
    char_list.remove(char_list[-1])
    char_list.remove(char_list[-2])
    char_list.remove(char_list[-3])

# finding the word max or min
min_max = 0
flag = True


if ((char_list[0] == 'm' or char_list[0] =='M') and (char_list[1] == 'a' or char_list[1] == 'A') and\
(char_list[2] == 'x' or char_list[2] == 'X')):
    min_max = 1
    print(min_max)
elif ((char_list[0] == 'm' or char_list[0] =='M') and (char_list[1] == 'I' or char_list[1] == 'i') and\
(char_list[2] == 'n' or char_list[2] == 'N')):
    min_max = -1
    print(min_max)
else:
    print('Error, max or min does not exist in the file')
    exit(1)


# checking if st exists in my problem

counter1 = 0
counter2 = 0

for ch in range(len(char_list)):
    if counter1 == 1:
        counter2 += 1
        if not(char_list[ch] == 's' and char_list[ch+1] == 't') or (char_list[ch] == 's.' and char_list[ch+1] == 't.')\
            or (char_list[ch] == 'S' and char_list[ch+1] == 'T') or (char_list[ch] == 'S.' and char_list[ch+1] == 'T.') and counter2 <2 :
            print('Error, st does not exist in the file')
            exit(1)
            break

        if char_list[ch] == '\n' and counter2 < 3:
            counter1 += 1

# checking if all my factors have a sign in Z

x_counter = 0
sign_counter = 0
line_counter = 0
symbol_counter = 0


for ch in range(len(char_list)):
    if char_list[ch] == 'x':
        x_counter += 1
    if char_list[ch] == '+' or char_list[ch] == '-':
        sign_counter += 1
    if char_list[ch] == '\n':
        line_counter += 1
    if char_list[ch] == '=':
        symbol_counter += 1

if min_max == 1:
    if x_counter - 1 != sign_counter:
        print('Error! Sign missing')
        exit(1)
elif min_max == -1:
    if x_counter != sign_counter:
        print('Error! Sign missing')
        exit(1)

if line_counter - 2 != symbol_counter:
    print('Error! Symbols missing')
    exit(1)

# checking if the right part of the equation exists

for ch in range(len(char_list)):
    if char_list[ch] == '=':
        if char_list[ch+1] not in ['-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
            print('Error! The right part of the equation does not exist')
            exit(1)


# creating the equin table
equin = []

for ch in range(len(char_list)):
    if char_list[ch] == '<' and char_list[ch+1] == '=':
        equin.append('-1')
    elif char_list[ch] == '>' and char_list[ch+1] == '=':
        equin.append('1')
    elif (char_list[ch] == '=' and char_list[ch-1] != '>') and (char_list[ch] == '=' and char_list[ch-1] != '<'):
        equin.append('0')

# creating the b table
b = []
temp_b = []
for ch in range(len(char_list)):

    if char_list[ch] == '=':
        temp_b = []
        while char_list[ch + 1] is not '\n':
            temp_b.append(char_list[ch +1])
            ch += 1
        for item in temp_b:
            b.append(item)
        b.append('/')


# removing the word max or min

char_list.remove(char_list[0])
char_list.remove(char_list[0])
char_list.remove(char_list[0])

# creating the C table
c = []
i = 0
flag_cof = True
c_index = []
sym = 1
pos_n = 0

while i < len(char_list):
    if char_list[i] == '\n':
        pos_n = i
        break

    if char_list[i] == '+':
        sym = 1
        i += 1
    elif char_list[i] == '-':
        sym = -1
        i += 1

    if 48 <= ord(char_list[i]) <= 57:
        temp_dig = []
        sum = 0

        while 48 <= ord(char_list[i]) <= 57:
            temp_dig.append(int(char_list[i]))
            i += 1
        for item in temp_dig:
            sum += item * pow(10, len(temp_dig)-1)
        if flag_cof:
            flag_cof = False
            if char_list[i] == 'x' or char_list[i] == 'X':
                sum = sum * sym
                sym = 1
                c.append(sum)
            i += 1
        else:
            flag_cof = True
            c_index.append(sum)
    elif char_list[i] == 'x' or char_list[i] == 'X':
        if flag_cof :
            flag_cof = False
            c.append(sym)
            sym = 1
        i += 1




# creating the A table
a = []
index = pos_n + 3

for line in range(len(equin)):
    flag_coef = True
    a_coef = []
    index_a = []
    sym = 1
    while index < len(char_list):
        if char_list[index] == '<' or char_list[index] == '>':
            index += 1
            while char_list[index] is not '\n':
                index += 1
            break
        if char_list[index] == '+':
            sym = 1
            index += 1
        elif char_list[index] == '-':
            sym = -1
            index += 1

        if 48 <= ord(char_list[index]) <= 57:
            temp_digs = []
            sum = 0

            while 48 <= ord(char_list[index]) <= 57:
                temp_digs.append(int(char_list[index]))
                index += 1
            for value in temp_digs:
                sum += value * pow(10, len(temp_digs) - 1)
            if flag_coef:
                flag_coef = False
                if char_list[index] == 'x' or char_list[index] == 'X':
                    sum = sum * sym
                    sym = 1
                    a_coef.append(sum)
                index += 1
            else:
                flag_coef = True
                index_a.append(sum)

        elif char_list[index] == 'x' or char_list[index] == 'X':
            if flag_coef:
                flag_coef = False
                a_coef.append(sym)
            index += 1
    temp = []
    count_appen = 0
    for i in range(1, len(c) + 1):
        if i in index_a:
            temp.append(a_coef[count_appen])
            count_appen += 1
        else:
            temp.append(0)
    index += 1
    a.append(temp)


# create the output file

filename = 'LP02.txt'
with open(filename, 'w') as f:
    if min_max == -1:
        f.write('min\n')
    else:
        f.write('max\n')
    f.write('c=[\t')

    for item in c:
        f.write('%s\t' % item)
    f.write(']\n')
    f.write('A=[\t')
    for index, item in enumerate(a):
        if index == len(a) - 1:
            f.writelines('%s]\n' % item)
        else:
            f.writelines('%s\n\t' % item)
    f.write('\nequin=[\t')
    for item in equin:
        f.write('%s\t' % item)
    f.write(']\n')

    f.write('\nb=[\t')
    for item in b:
        if item is not '/':
            f.write('%s' % item)
        else:
            f.write('\t')
    f.write(']\n')



